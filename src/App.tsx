import { useState } from "react";
import "./styles.css";

const images = [
  "https://asiasociety.org/sites/default/files/styles/1200w/public/2020-06/Kazakhstan%20Key%20Visual%20NEW.jpg",
  "https://cdn2.wanderlust.co.uk/media/1010/shutterstock_1236828025.jpg?anchor=center&mode=crop&width=1440&height=0&format=auto&quality=90&rnd=132804346000000000",
];

function App() {
  return <Slideshow slides={images} />;
}

interface SlideshowProps {
  slides: string[];
}

const Slideshow: React.FC<SlideshowProps> = ({ slides }) => {
  const [currentSlide, setCurrentSlide] = useState(slides[0]);
  const [rotate, setRotate] = useState(false);

  const handleRotate = () => {
    setRotate(true), setTimeout(() => setRotate(false), 1300);
  };

  return (
    <>
      <div className="slideshow">
        <div className="slideshow__slide slide">
          <div
            className={`slide__element slide__1 ${rotate ? "rotating" : ""}`}
          >
            <img className="slide__image" src={currentSlide} />
          </div>
          <div
            className={`slide__element slide__2 ${rotate ? "rotating" : ""}`}
          >
            <img className="slide__image" src={currentSlide} />
          </div>
          <div
            className={`slide__element slide__3 ${rotate ? "rotating" : ""}`}
          >
            <img className="slide__image" src={currentSlide} />
          </div>
        </div>
      </div>

      <button onClick={handleRotate}>Rotate</button>
    </>
  );
};

export default App;
